#!/usr/bin/python2
#import ScrolledText
import Tkinter as tk
from hashlib import md5

NUMBER_OF_CHAR_LINE = 15	
class SampleApp(tk.Tk):
	def __init__(self):

		tk.Tk.__init__(self)
		t = tk.Text(self,height=5)
		t.insert(tk.INSERT,'--------Enter path to file:--------')
		t.pack()
		self.entry = tk.Entry(self,textvariable=20)
		#self.entry = tk.ScrolledText.ScrolledText(self, wrap=tk.WORD)
		self.button = tk.Button(self, text="Get", command=self.on_button,height = 2, width = 70)
		self.anotherButton = tk.Button(self,text='Exit',command=self.exitme,height = 2, width = 70)
		self.anotherButton.pack()
		self.button.pack()
		self.entry.pack()

	def exitme(self):
		self.destroy()
		self.quit()
	
	def on_button(self): 
		ls= []
		data = self.entry.get()
		self.n = tk.Tk()
		self.t = tk.Text(self.n)
		s=0
		st=''
		base =0
		var_is_string=False

		try:
			f = open(data,'r')
			file = f.read()
		except:
			print data
			self.exitme()
			sys.exit()
		finally:
			ls=[]
			for i in range(0,len(file),NUMBER_OF_CHAR_LINE):
				line =''
				j=i
				try:
					while(j < i+NUMBER_OF_CHAR_LINE):
						line += str(hex(ord(file[j])))[2:].zfill(2) + ' '
						j+=1
				except:
					pass
				j=i
				if len(line) < 45:
					line += ' '*(45-len(line)) + '||'
				else:
					line += '||'
				try:
					while(j < i+NUMBER_OF_CHAR_LINE):
						ch = file[j]
						if(127 > ord(ch) > 31):
							line += ch
						else:
							line += '.'	
						j+=1
				except:
					pass	
				line += '\n'
				ls.append(line)

		for x in ls:
			self.t.insert(tk.END, x + '\n')
		self.t.pack()
		self.n.mainloop()

app = SampleApp()
app.geometry("400x400")
app.mainloop()
